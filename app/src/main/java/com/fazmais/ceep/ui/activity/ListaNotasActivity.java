package com.fazmais.ceep.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.fazmais.ceep.R;
import com.fazmais.ceep.ui.activity.model.Nota;
import com.fazmais.ceep.ui.database.CeepDataBase;
import com.fazmais.ceep.ui.database.NotaDao;
import com.fazmais.ceep.ui.recyclerview.adapter.ListaNotasAdapter;
import com.fazmais.ceep.ui.recyclerview.adapter.listener.OnItemClickListener;
import com.fazmais.ceep.ui.recyclerview.helper.callback.NotaItemTouchHelperCallback;

import static com.fazmais.ceep.ui.activity.Nota_Activity_Constantes.CHAVE_NOTA;
import static com.fazmais.ceep.ui.activity.Nota_Activity_Constantes.CHAVE_POSICAO;
import static com.fazmais.ceep.ui.activity.Nota_Activity_Constantes.CODIGO_REQUISICAO_ALTERA_NOTA;
import static com.fazmais.ceep.ui.activity.Nota_Activity_Constantes.CODIGO_REQUISICAO_INSERE_NOTA;
import static com.fazmais.ceep.ui.activity.Nota_Activity_Constantes.POSICAO_INVALIDA;

public class ListaNotasActivity extends AppCompatActivity {

    public static final String TITULO_APPBAR = "Notas";
    private ListaNotasAdapter adapter;
    private NotaDao dao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_notas);
        configuraReciclerView();
        configura_insere_nota();
        setTitle(TITULO_APPBAR);
        dao = CeepDataBase.getInstance(this).getNotaDAO();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.menu_lista_nota_apaga, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (ehMenuNotaApaga(item)){
            if(possuiNotas()) {
                apagaTodasNotas();
            }else {
                Toast.makeText(this, getString(R.string.lista_vazia), Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean possuiNotas() {
        return dao.todos().size()>0;
    }

    private boolean ehMenuNotaApaga(@NonNull MenuItem item) {
        return item.getItemId()== R.id.menu_lista_nota_ic_apaga;
    }

    private void apagaTodasNotas() {
        new AlertDialog.Builder(this).setTitle(R.string.apaga_todas_notas)
                .setMessage(R.string.confirma_apagar_todas_notas)
                .setPositiveButton(R.string.sim, (dialog, which) -> {
                    dao.removeTodos(dao.todos());
                    adapter.removeTodos();
                }).setNegativeButton(R.string.nao,null).show();
    }

    private void configura_insere_nota() {
        TextView insereUmaNota = findViewById(R.id.lista_notas_insere_nota);
        insereUmaNota.setOnClickListener(v -> vaiParaFormularioNotaActivityInsere());
    }

    private void vaiParaFormularioNotaActivityInsere() {
        Intent iniciaFormularioNota = new Intent(ListaNotasActivity.this, FormularioNotaActivity.class);
        startActivityForResult(iniciaFormularioNota, CODIGO_REQUISICAO_INSERE_NOTA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultadoOk(resultCode)) {
            if (ehResultadoInsereNota(requestCode, data)) {
                Nota notaRecebida = (Nota) data.getSerializableExtra(CHAVE_NOTA);
                adiciona(notaRecebida);
            }

            if (ehResultadoAlteraNota(requestCode, data)) {
                Nota notaRecebida = (Nota) data.getSerializableExtra(CHAVE_NOTA);
                int posicaoRecebida = data.getIntExtra(CHAVE_POSICAO, POSICAO_INVALIDA);
                if (ehPosicaoValida(posicaoRecebida)) {
                    altera(notaRecebida, posicaoRecebida);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void altera(Nota nota, int posicao) {
        dao.altera(nota);
        adapter.altera(posicao, nota);
    }

    private boolean ehPosicaoValida(int posicaoRecebida) {
        return posicaoRecebida > POSICAO_INVALIDA;
    }

    private boolean ehResultadoAlteraNota(int requestCode, Intent data) {
        return ehCodigoRequisicaoAlteraNota(requestCode) && possuiExtra(data);
    }

    private boolean ehCodigoRequisicaoAlteraNota(int requestCode) {
        return requestCode == CODIGO_REQUISICAO_ALTERA_NOTA;
    }

    private void adiciona(Nota nota) {
        int notaId = dao.insere(nota).intValue();
        nota.setId(notaId);
        adapter.adiciona(nota);
    }

    private boolean ehResultadoInsereNota(int requestCode, Intent data) {
        return ehCodigoRequisicaoNota(requestCode) && possuiExtra(data);
    }

    private boolean possuiExtra(Intent data) {
        return data.hasExtra(CHAVE_NOTA);
    }

    private boolean resultadoOk(int resultCode) {
        return resultCode == Activity.RESULT_OK;
    }

    private boolean ehCodigoRequisicaoNota(int requestCode) {
        return requestCode == CODIGO_REQUISICAO_INSERE_NOTA;
    }

    private void configuraReciclerView() {
        RecyclerView listaNotas = findViewById(R.id.lista_notas_recyclerview);
        configuraAdapter( listaNotas);
        configuraItemTouchHelper(listaNotas);
    }

    private void configuraItemTouchHelper(RecyclerView listaNotas) {
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new NotaItemTouchHelperCallback(adapter, this));
        itemTouchHelper.attachToRecyclerView(listaNotas);
    }

    private void configuraAdapter( RecyclerView listaNotas) {
        adapter = new ListaNotasAdapter( this);
        listaNotas.setAdapter(adapter);
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(Nota nota, int posicao) {
                vaiParaFormularioNotaActivityAltera(nota, posicao);

            }
        });
    }

    private void vaiParaFormularioNotaActivityAltera(Nota nota, int posicao) {
        Intent abreFormularioComNota = new Intent(ListaNotasActivity.this, FormularioNotaActivity.class);
        abreFormularioComNota
                .putExtra(CHAVE_NOTA, nota)
                .putExtra(CHAVE_POSICAO, posicao);
        startActivityForResult(abreFormularioComNota, CODIGO_REQUISICAO_ALTERA_NOTA);
    }

}