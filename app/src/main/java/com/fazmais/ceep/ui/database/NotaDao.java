package com.fazmais.ceep.ui.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.fazmais.ceep.ui.activity.model.Nota;

import java.util.List;

@Dao
public interface NotaDao {

    @Query("SELECT * FROM Nota")
    List<Nota> todos();

    @Update
    void altera(Nota nota);

    @Insert
    Long insere(Nota nota);

    @Delete
    void removeTodos(List<Nota> todasNotas);

    @Delete
    void remove(Nota nota);

   @Update
   void troca(Nota... nota);
}
