package com.fazmais.ceep.ui.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.fazmais.ceep.ui.activity.model.Nota;

@Database(entities = Nota.class, version = 1, exportSchema = false)
public abstract class CeepDataBase extends RoomDatabase {

    public abstract NotaDao getNotaDAO();

    public static CeepDataBase getInstance(Context context) {
       return Room.databaseBuilder(context, CeepDataBase.class, "ceep.db")
               .allowMainThreadQueries().build();
    }
}


