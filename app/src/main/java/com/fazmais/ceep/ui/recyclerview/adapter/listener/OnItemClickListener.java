package com.fazmais.ceep.ui.recyclerview.adapter.listener;

import com.fazmais.ceep.ui.activity.model.Nota;

public interface OnItemClickListener {

    void onItemClick(Nota nota, int posicao);

}
