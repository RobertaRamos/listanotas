package com.fazmais.ceep.ui.recyclerview.helper.callback;

import android.content.Context;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.fazmais.ceep.ui.activity.model.Nota;
import com.fazmais.ceep.ui.database.CeepDataBase;
import com.fazmais.ceep.ui.database.NotaDao;
import com.fazmais.ceep.ui.recyclerview.adapter.ListaNotasAdapter;

import java.util.List;

public class NotaItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final ListaNotasAdapter adapter;
    private List<Nota> notas;
    private final NotaDao dao;


    private void todasNotas() {
        notas = dao.todos();
    }

    public NotaItemTouchHelperCallback(ListaNotasAdapter adapter, Context context) {
        this.adapter = adapter;
        dao = CeepDataBase.getInstance(context).getNotaDAO();
    }


    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int marcacoesDeslize = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
        int marcacoesDeArrastar = ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT;
        return makeMovementFlags(marcacoesDeArrastar, marcacoesDeslize);

    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        int posicaoInicial = viewHolder.getAdapterPosition();
        int posicaoFinal = target.getAdapterPosition();
        trocaNotas(posicaoInicial, posicaoFinal);

        return true;
    }

    private void trocaNotas(int posicaoInicial, int posicaoFinal) {
        todasNotas();
        Nota posicaoNotaInicial = notas.get(posicaoInicial);
        Nota posicaoNotafinal = notas.get(posicaoFinal);
        atualizaId(posicaoNotaInicial, posicaoNotafinal);
        dao.troca(posicaoNotaInicial, posicaoNotafinal);
        adapter.troca(posicaoInicial, posicaoFinal);
    }

    private void atualizaId(Nota posicaoNotaInicial, Nota posicaoNotafinal) {
        int id;
        id = posicaoNotafinal.getId();
        posicaoNotafinal.setId(posicaoNotaInicial.getId());
        posicaoNotaInicial.setId(id);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        todasNotas();
        int posicaoDaNotaDeslizada = viewHolder.getAdapterPosition();
        Nota notaDeslizada = notas.get(posicaoDaNotaDeslizada);
        removeNota(posicaoDaNotaDeslizada, notaDeslizada);

    }

    private void removeNota(int posicao, Nota nota) {
        dao.remove(nota);
        adapter.remove(posicao);
    }
}
