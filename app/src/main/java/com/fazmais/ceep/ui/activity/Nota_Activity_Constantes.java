package com.fazmais.ceep.ui.activity;

public interface Nota_Activity_Constantes {
    String CHAVE_NOTA = "nota";
    int CODIGO_REQUISICAO_INSERE_NOTA = 1;
    int CODIGO_REQUISICAO_ALTERA_NOTA = 2;
    String CHAVE_POSICAO = "posicao";
    int POSICAO_INVALIDA = -1;
}
